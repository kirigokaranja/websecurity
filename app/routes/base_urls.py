import requests
import random
import africastalking

from flask import jsonify, request, session
from datetime import datetime

from routes import app, db
from database.user import User
from database.user_sessions import UserSession

@app.route("/", methods=["GET"])
def index():
    if 'session_id' in session:
        return 'Logged in as %s' % escape(session['session_id'])
    return 'You are not logged in'

@app.route("/register", methods = ["POST"])
def register_user():
    phone_number = request.json.get("phone_number")
    email = request.json.get("email")
    password = request.json.get("password")
    user = User(
        user_phone = phone_number,
        user_email = email
    )
    user.hash_password(password)
    print("User", user.return_json())
    db.session.add(user)
    try:
        db.session.commit()
        db.session.close()
        message = ["User has been added successfully"]
        return jsonify({
        "message":message
        }),201

    except Exception as e:
        db.session.rollback()
        db.session.close()
        message = ["User not added"]
        return jsonify({
        "message":message,
        "exception": str(e)
        }),406
    
@app.route("/login", methods= ["POST"])
def login_user():

        phone_number = request.json.get("phone_number")
        password = request.json.get("password")
        if phone_number is None or password is None:
            return "Missing arguments", 400
        user = User.query.filter_by(user_phone=phone_number).first()
        hashed = user.user_password
        if user and user.verify_password(password, hashed):
            code = random.randint(1000,9999)
            # Initialize SDK
            username = "sandbox"   
            api_key = "bb3c9c67792a3f9ccf1f38c87117d96e80051f65b007eb9b98922f73da5924fe"      
            africastalking.initialize(username, api_key)

            # Initialize a service e.g. SMS
            sms = africastalking.SMS

            recipient = ["+" + phone_number]
            print(recipient)
            message = "Your activation code is {}".format(code)
            sender = "Web Security Application"

            try:
                response = sms.send(message, recipient)
                id = user.user_id
                session_code = random.randint(100000, 999999)
                session['session_id'] = session_code
                sessions = UserSession(
                    user_id = id,
                    session = session_code,
                    created_date = datetime.now(),
                    activation_code = code,
                    status = "pending"
                )
                print("Session", sessions.return_json())
                db.session.add(sessions)
                try:
                    db.session.commit()
                    db.session.close()
                    return jsonify({
                    "message": "user verified",
                    "user_id": id,
                    "user": user.user_email,
                    "session_id": session_code,
                    "cookies location": "C: Users username AppData Local Microsoft Windows INetCookies"
                }), 200

                except Exception as e:
                    db.session.rollback()
                    db.session.close()
                    message = ["Session not added"]
                    return jsonify({
                    "message":message,
                    "exception": str(e)
                    }),406

            except Exception as e:
                return jsonify({
                "message": e
                 }), 406

            
 
        else:
            return jsonify({
                "message": "Incorrect Username or Password. Please try again"
            }), 406


@app.route("/sessions/<users_id>", methods = ["GET"])
def user_sessions(users_id):
    sessions = UserSession.query.filter_by(user_id=users_id).all()
    if not sessions:
        return jsonify({
            "message": "Selected user's sessions dont exist"
        }),404

    else:
        data = []
        for single in sessions:
            data.append(single.return_json())

        return jsonify({
            "message": "User details fetched successfully",
            "data": data
        }),200

@app.route("/activate", methods = ["POST"])
def activate_code():
    code = request.json.get("activation_code")
    session_id = request.json.get("session_id")
    session = UserSession.query.filter_by(session=session_id).first()
    if not session:
        return jsonify({
            "message": "Session does not exist"
        }),404

    else:
        if code == session.activation_code:
            session.status = "Activated"
            try:
                db.session.commit()
                db.session.close()

                return jsonify({
                     "message": "Login successful"
                }),200

            except Exception as e:
                db.session.rollback()
                db.session.close()
                return jsonify({
                     "message": "Login Unsuccesful",
                     "error": str(e)
                }),404
        else:
           return jsonify({
            "message": "Activation code does not match"
        }),406
        
    