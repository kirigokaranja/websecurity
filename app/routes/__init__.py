from flask import Flask, json, jsonify, request, redirect, session
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy

from datetime import datetime


app = Flask(__name__)
CORS(app)

SESSION_TYPE = 'redis'
app.secret_key = "dty89njhsYUHv474"

# PostgreSQL
app.config["SQLALCHEMY_DATABASE_URI"] = "postgresql://postgres:500Horsepower?@188.166.151.87:5432/websecurity"
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] =  False
app.config["JSON_SORT_KEYS"] = False
app.config["JSONIFY_PRETTYPRINT_REGULAR"] = False
app.config["SECRET_KEY"] = "dty89njhsYUHv474"

db = SQLAlchemy(app, session_options={"expire_on_commit": False})

@app.after_request
def append_timestamp_to_json(response):
	data = response.json

	if data:
		access_time = datetime.now()

		data["access_time"] = access_time

		response.set_data(json.dumps(data))

	else:
		pass

	return response

from routes import base_urls
