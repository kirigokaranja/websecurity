#import cherrypy

from routes import app


if __name__ == '__main__':
    # # # Mount the application
    # cherrypy.tree.graft(app, "/")

    # # # Unsubscribe the default server
    # cherrypy.server.unsubscribe()

    # # # Instantiate a new server object
    # server = cherrypy._cpserver.Server()

    # # # Configure the server object
    # server.socket_host = "0.0.0.0"
    # server.socket_port = 9000

    # # # Subscribe this server
    # server.subscribe()

    # cherrypy.engine.start()
    # cherrypy.engine.block()

    app.run(port=1010, host='127.0.0.1', debug=True)
