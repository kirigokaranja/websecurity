import os
from datetime import datetime
import scrypt
# from passlib.hash import scrypt
from routes import db

class User(db.Model):
    __tablename__ = "users"
    user_id = db.Column(db.Integer, primary_key = True)
    user_phone = db.Column(db.String(100), unique = True, nullable = False, index = True)
    user_email = db.Column(db.String(100), unique = True, nullable = False, index = True)
    user_password = db.Column(db.Text, nullable = False)

    # Relationships
    user_sessions_rel = db.relationship("UserSession", backref = "rel_user_sessions", lazy = "dynamic")

    def hash_password(self, password):
        passw = scrypt.hash(password, "dty89njhsYUHv474")
        self.user_password = passw.decode('utf-8', "ignore")

    def verify_password(self, password, hashed_password):
        try:
            hashed = scrypt.hash(password, "dty89njhsYUHv474")
            hashed = hashed.decode('utf-8', "ignore")
            if hashed == hashed_password:
                return True
            else:
                return False
        except scrypt.error:
            return False
    

    def __init__(self, user_phone=None, user_password = None, user_email = None):
        self.user_phone = user_phone
        self.user_password = user_password
        self.user_email = user_email
        
    def __str__(self):
        return "<User {}, {}>",format(self.user_phone, self.user_email)

    def __repr__(self):
        return "<User {}, {}>",format(self.user_phone, self.user_email)

    def return_json(self):
        return{
            "user_id": self.user_id,
            "user_phone": self.user_phone,
            "user_email": self.user_email,
            "user_password": self.user_password,
            "sessions": [
                session.return_json() for session in self.user_sessions_rel
            ]

        }
