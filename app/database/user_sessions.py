
from datetime import datetime

from routes import db

class UserSession(db.Model):
    __tablename__ = "sessions"
    session_id = db.Column(db.Integer, primary_key = True)
    user_id = db.Column(db.Integer, db.ForeignKey("users.user_id"), index = True, nullable = False)
    session = db.Column(db.String(100), unique = True, nullable = False, index = True)
    created_date = db.Column(db.String(100), nullable = False)
    activation_code = db.Column(db.String(100), nullable = False)
    status = db.Column(db.String(100), nullable = False)

    def __init__(self, user_id, session, created_date, activation_code, status):
        self.user_id = user_id
        self.session = session
        self.created_date = created_date
        self.activation_code = activation_code
        self.status = status
    
    def __str__(self):
        return "<Session {}, {}, {}, {}, {}>".format(self.user_id,self.session,self.created_date, self.activation_code,self.status)

    def __repr__(self):
        return "<Session {}, {}, {}, {}, {}>".format(self.user_id,self.session,self.created_date,self.activation_code,self.status)
      
    def return_json(self):
        return {
            "session_id": self.session_id,
            "user_id": self.user_id,
            "session": self.session,
            "created_date": self.created_date,
            "activation_code": self.activation_code,
            "status": self.status
        }
